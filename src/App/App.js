import { Navbar } from "../components/navbar/Navbar";
import { Page } from "../components/page/Page";
import "../App/App.scss";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Page />
    </div>
  );
}

export default App;
