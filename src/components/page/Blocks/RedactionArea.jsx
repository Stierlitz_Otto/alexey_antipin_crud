import React from "react";
import "./RedactionArea.scss";

export const RedactionArea = ({ item, turnOn, index, setvalueText, state_obj }) => {

    function changeText(index, event) {
        const copy = Object.assign([], state_obj);
        copy[index].data.name = event.target.value;
        const valueText = copy[index].data.name;
        setvalueText(valueText)
    }

    return (
        <div className="Page__Block">
            {/* Режим показа */}
            <div className="Page__Text">{item.data?.name}</div>
            <div className="Page__Text">{item.data?.description}</div>
            {/* Режим редактирования */}
            <div className="Page__Block_Area">
                {
                    turnOn == true ? <textarea
                        maxLength={50}
                        placeholder="Текст пуст"
                        className="Page__TextRedaction"
                        onChange={(event) => changeText(index, event)}
                        value={item.data?.name} /> : <div></div>
                }
            </div>
        </div>
    )
}