import axios from "axios";

// Получение
export const getName = () => {
    return async (dispatch) => {
        try {
            const res = await axios.get("http://178.128.196.163:3000/api/records")
            dispatch({ type: 'GET_NAME', payload: res.data })
        } catch (error) {
            console.log(error)
        }
    }
}

// Удаление
export const deleteName = ({ _id }) => {
    return async (dispatch) => {
        try {
            await axios.delete(`http://178.128.196.163:3000/api/records/${_id}`)
            dispatch({ type: 'DELETE_NAME', payload: { _id } })
        } catch (error) {
            console.log(error)
        }
    }
};

// Создание
export const addName = ({ name }) => {
    return async (dispatch) => {
        try {
            let res = await axios.put(`http://178.128.196.163:3000/api/records`,
                { "data": { name } })
            let data = res.data;
            dispatch({ type: 'ADD_NAME', payload: data });
        } catch (error) {
            console.log(error)
        }
    }
};

// Изменение
export const EditName = ({ _id, name }) => {
    return async (dispatch) => {
        try {
            let res = await axios.post(`http://178.128.196.163:3000/api/records/${_id}`,
                {
                    "data": { name }
                })
            dispatch({ type: 'EDIT_NAME', payload: res.data });
        } catch (error) {
            console.log(error)
        }
    }
};
